LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := opencv_card

LOCAL_MODULE_FILENAME := libopencv_card

ifeq ($(USE_ARM_MODE),1)
LOCAL_ARM_MODE := arm
endif

LOCAL_SRC_FILES := \
CardDetector.cpp \

LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH) \
                        $(LOCAL_PATH)/include \


LOCAL_C_INCLUDES := $(LOCAL_PATH) \
                        $(LOCAL_PATH)/include \


LOCAL_WHOLE_STATIC_LIBRARIES := android_calib3d
LOCAL_EXPORT_LDLIBS := -lGLESv1_CM \
                       -lGLESv2 \
                       -lEGL \
                       -llog \
                       -landroid


LOCAL_CPPFLAGS := -Wno-extern-c-compat

LOCAL_EXPORT_CPPFLAGS := -Wno-extern-c-compat

LOCAL_CFLAGS   :=  -DUSE_FILE32API
LOCAL_CFLAGS   +=  -fexceptions
LOCAL_CPPFLAGS := -Wno-deprecated-declarations -Wno-extern-c-compat
LOCAL_EXPORT_CFLAGS   := -DUSE_FILE32API
LOCAL_EXPORT_CPPFLAGS := -Wno-deprecated-declarations -Wno-extern-c-compat
include $(BUILD_STATIC_LIBRARY)
#==============================================================

include $(CLEAR_VARS)

LOCAL_MODULE := opencv3_static
LOCAL_MODULE_FILENAME := libopencv3

#OPENCV3
#==============================================================
$(call import-module,opencv3/prebuilt/android_calib3d)
$(call import-module,opencv3/prebuilt/android_core)
$(call import-module,opencv3/prebuilt/android_features2d)
$(call import-module,opencv3/prebuilt/android_flann)
$(call import-module,opencv3/prebuilt/android_hal)
$(call import-module,opencv3/prebuilt/android_highgui)
$(call import-module,opencv3/prebuilt/android_imgcodecs)
$(call import-module,opencv3/prebuilt/android_imgproc)
$(call import-module,opencv3/prebuilt/android_ml)
$(call import-module,opencv3/prebuilt/android_objdetect)
$(call import-module,opencv3/prebuilt/android_photo)
$(call import-module,opencv3/prebuilt/android_shape)
$(call import-module,opencv3/prebuilt/android_stitching)
$(call import-module,opencv3/prebuilt/android_superres)
$(call import-module,opencv3/prebuilt/android_ts)
$(call import-module,opencv3/prebuilt/android_video)
$(call import-module,opencv3/prebuilt/android_videoio)
$(call import-module,opencv3/prebuilt/android_videostab)
