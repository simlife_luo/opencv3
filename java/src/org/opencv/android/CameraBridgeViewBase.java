package org.opencv.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import org.opencv.R;
import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This is a basic class, implementing the interaction with Camera and OpenCV library.
 * The main responsibility of it - is to control when camera can be enabled, process the frame,
 * call external listener to make any adjustments to the frame and then draw the resulting
 * frame to the screen.
 * The clients shall implement CvCameraViewListener.
 */
public abstract class CameraBridgeViewBase extends SurfaceView implements SurfaceHolder.Callback {

    private static final String TAG = "CameraBridge";
    private static final int MAX_UNSPECIFIED = -1;
    private static final int STOPPED = 0;
    private static final int STARTED = 1;

    private int mState = STOPPED;
    private Bitmap mCacheBitmap;
    private CvCameraViewListener2 mListener;
    private boolean mSurfaceExist;
    private Object mSyncObject = new Object();

    protected int mFrameWidth;
    protected int mFrameHeight;
    protected int mMaxHeight;
    protected int mMaxWidth;
    protected int mPreviewFormat = RGBA;
    protected int mCameraIndex = CAMERA_ID_ANY;
    protected boolean mEnabled;
    protected FpsMeter mFpsMeter = null;

    public static final int CAMERA_ID_ANY = -1;
    public static final int CAMERA_ID_BACK = 99;
    public static final int CAMERA_ID_FRONT = 98;
    public static final int RGBA = 1;
    public static final int GRAY = 2;
    private boolean clean;
    protected float mScale;
    private SnapCallback snapCallback;


    public CameraBridgeViewBase(Context context, int cameraId) {
        super(context);
        mCameraIndex = cameraId;
        getHolder().addCallback(this);
        mMaxWidth = MAX_UNSPECIFIED;
        mMaxHeight = MAX_UNSPECIFIED;
    }

    public CameraBridgeViewBase(Context context, AttributeSet attrs) {
        super(context, attrs);

        int count = attrs.getAttributeCount();
        Log.d(TAG, "Attr count: " + Integer.valueOf(count));

        TypedArray styledAttrs = getContext().obtainStyledAttributes(attrs, R.styleable.CameraBridgeViewBase);
        if (styledAttrs.getBoolean(R.styleable.CameraBridgeViewBase_show_fps, false))
            enableFpsMeter();

        mCameraIndex = styledAttrs.getInt(R.styleable.CameraBridgeViewBase_camera_id, -1);

        getHolder().addCallback(this);
        mMaxWidth = MAX_UNSPECIFIED;
        mMaxHeight = MAX_UNSPECIFIED;
        styledAttrs.recycle();
    }

    /**
     * Sets the camera index
     *
     * @param cameraIndex new camera index
     */
    public void setCameraIndex(int cameraIndex) {
        this.mCameraIndex = cameraIndex;
    }



    public interface SnapCallback {
        public void snapLast(Mat last);
    }

    public interface CvCameraViewListener {
        /**
         * This method is invoked when camera preview has started. After this method is invoked
         * the frames will start to be delivered to client via the onCameraFrame() callback.
         *
         * @param width  -  the width of the frames that will be delivered
         * @param height - the height of the frames that will be delivered
         */
        public void onCameraViewStarted(int width, int height);

        /**
         * This method is invoked when camera preview has been stopped for some reason.
         * No frames will be delivered via onCameraFrame() callback after this method is called.
         */
        public void onCameraViewStopped();

        /**
         * This method is invoked when delivery of the frame needs to be done.
         * The returned values - is a modified frame which needs to be displayed on the screen.
         * TODO: pass the parameters specifying the format of the frame (BPP, YUV or RGB and etc)
         */
        public Mat onCameraFrame(Mat inputFrame);
    }

    public interface CvCameraViewListener2 {
        /**
         * This method is invoked when camera preview has started. After this method is invoked
         * the frames will start to be delivered to client via the onCameraFrame() callback.
         *
         * @param width  -  the width of the frames that will be delivered
         * @param height - the height of the frames that will be delivered
         */
        public void onCameraViewStarted(int width, int height);

        /**
         * This method is invoked when camera preview has been stopped for some reason.
         * No frames will be delivered via onCameraFrame() callback after this method is called.
         */
        public void onCameraViewStopped();

        /**
         * This method is invoked when delivery of the frame needs to be done.
         * The returned values - is a modified frame which needs to be displayed on the screen.
         * TODO: pass the parameters specifying the format of the frame (BPP, YUV or RGB and etc)
         */
        public Mat onCameraFrame(CvCameraViewFrame inputFrame);
    }

    ;

    protected class CvCameraViewListenerAdapter implements CvCameraViewListener2 {
        public CvCameraViewListenerAdapter(CvCameraViewListener oldStypeListener) {
            mOldStyleListener = oldStypeListener;
        }

        public void onCameraViewStarted(int width, int height) {
            mOldStyleListener.onCameraViewStarted(width, height);
        }

        public void onCameraViewStopped() {
            mOldStyleListener.onCameraViewStopped();
        }

        public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
            Mat result = null;
            switch (mPreviewFormat) {
                case RGBA:
                    result = mOldStyleListener.onCameraFrame(inputFrame.rgba());
                    break;
                case GRAY:
                    result = mOldStyleListener.onCameraFrame(inputFrame.gray());
                    break;
                default:
                    Log.e(TAG, "Invalid frame format! Only RGBA and Gray Scale are supported!");
            }
            ;

            return result;
        }

        public void setFrameFormat(int format) {
            mPreviewFormat = format;
        }

        private int mPreviewFormat = RGBA;
        private CvCameraViewListener mOldStyleListener;
    }

    ;

    /**
     * This class interface is abstract representation of single frame from camera for onCameraFrame callback
     * Attention: Do not use objects, that represents this interface out of onCameraFrame callback!
     */
    public interface CvCameraViewFrame {

        /**
         * This method returns RGBA Mat with frame
         */
        public Mat rgba();

        /**
         * This method returns single channel gray scale Mat with frame
         */
        public Mat gray();

        public Mat bgr();
    }

    private Point offset = new Point();

    private Mat mask = new Mat();

    private double matScale = 1;

    protected void setMaskMat(Mat mask) {
        this.mask.release();
        offset.x = 0;
        offset.y = 0;
        int h = 0;
        int w = 0;
        Mat maskLayer = new Mat();
        if (mask.channels() == 4) {
            List<Mat> layers = new ArrayList<Mat>();
            Core.split(mask, layers);
            layers.get(3).copyTo(maskLayer);
            for (Mat m : layers) {
                m.release();
            }
            List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
            Imgproc.threshold(maskLayer, maskLayer, 0, 255, Imgproc.THRESH_BINARY);
            Mat tmp = maskLayer.clone();
            Imgproc.findContours(tmp, contours, new Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_NONE);
            tmp.release();
            Point tl = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
            Point br = new Point(0, 0);
            for (MatOfPoint cnt : contours) {
                org.opencv.core.Rect bounds = Imgproc.boundingRect(cnt);
                if (bounds.x < tl.x) {
                    tl.x = bounds.x;
                }
                if (bounds.y < tl.y) {
                    tl.y = bounds.y;
                }
                if (bounds.x + bounds.width > br.x) {
                    br.x = bounds.x + bounds.width;
                }
                if (bounds.y + bounds.height > br.y) {
                    br.y = bounds.y + bounds.height;
                }
            }
            offset.x = Math.floor(tl.x) + 2;
            offset.y = Math.floor(tl.y) + 2;
            h = (int) Math.ceil(br.y - offset.y) - 4;
            w = (int) Math.ceil(br.x - offset.x) - 4;
            Mat roi = new Mat(mask, new org.opencv.core.Rect((int) offset.x, (int) offset.y, w, h));
            tmp = new Mat();
            Imgproc.cvtColor(roi, tmp, Imgproc.COLOR_BGRA2GRAY);
            Imgproc.threshold(tmp, tmp, 0, 255, Imgproc.THRESH_BINARY);
            Core.merge(Arrays.asList(tmp, tmp, tmp, tmp), maskLayer);
            tmp.release();
        } else if (mask.channels() == 1) {
            Core.merge(Arrays.asList(mask, mask, mask, mask), maskLayer);
            w = mask.width();
            h = mask.height();
        } else {
            Mat tmp = new Mat();
            Imgproc.cvtColor(mask, tmp, Imgproc.COLOR_BGR2GRAY);
            Core.merge(Arrays.asList(tmp, tmp, tmp, tmp), maskLayer);
            tmp.release();
            w = mask.width();
            h = mask.height();
        }
        if (maskLayer == null || maskLayer.empty() || h <= 0 || w <= 0) {
            this.mask = new Mat();
            return;
        }
        matScale = 1.0 * Math.max(w, h) / mFrameHeight;
        maskLayer.copyTo(this.mask);
        maskLayer.release();
    }

    public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
        Log.d(TAG, "call surfaceChanged event");
        synchronized (mSyncObject) {
            if (!mSurfaceExist) {
                mSurfaceExist = true;
                checkCurrentState();
            } else {
                /** Surface changed. We need to stop camera and restart with new parameters */
                /* Pretend that old surface has been destroyed */
                mSurfaceExist = false;
                checkCurrentState();
                /* Now use new surface. Say we have it now */
                mSurfaceExist = true;
                checkCurrentState();
            }
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        /* Do nothing. Wait until surfaceChanged delivered */
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        synchronized (mSyncObject) {
            mSurfaceExist = false;
            checkCurrentState();
        }
    }

    /**
     * This method is provided for clients, so they can enable the camera connection.
     * The actual onCameraViewStarted callback will be delivered only after both this method is called and surface is available
     */
    public void enableView() {
        this.setVisibility(SurfaceView.VISIBLE);
        synchronized (mSyncObject) {
            mEnabled = true;
            checkCurrentState();
        }
    }

    /**
     * This method is provided for clients, so they can disable camera connection and stop
     * the delivery of frames even though the surface view itself is not destroyed and still stays on the scren
     */
    public void disableView() {
        this.setVisibility(SurfaceView.INVISIBLE);
        synchronized (mSyncObject) {
            clean();
            mEnabled = false;
            checkCurrentState();
        }
    }

    public void clean() {
        Canvas canvas = getHolder().lockCanvas();
        if (canvas != null) {
            canvas.drawColor(0, android.graphics.PorterDuff.Mode.CLEAR);
            getHolder().unlockCanvasAndPost(canvas);
        }
    }

    /**
     * This method enables label with fps value on the screen
     */
    public void enableFpsMeter() {
        if (mFpsMeter == null) {
            mFpsMeter = new FpsMeter();
            mFpsMeter.setResolution(mFrameWidth, mFrameHeight);
        }
    }

    public void disableFpsMeter() {
        mFpsMeter = null;
    }

    /**
     * @param listener
     */

    public void setCvCameraViewListener(CvCameraViewListener2 listener) {
        mListener = listener;
    }

    public void setCvCameraViewListener(CvCameraViewListener listener) {
        CvCameraViewListenerAdapter adapter = new CvCameraViewListenerAdapter(listener);
        adapter.setFrameFormat(mPreviewFormat);
        mListener = adapter;
    }

    /**
     * This method sets the maximum size that camera frame is allowed to be. When selecting
     * size - the biggest size which less or equal the size set will be selected.
     * As an example - we set setMaxFrameSize(200,200) and we have 176x152 and 320x240 sizes. The
     * preview frame will be selected with 176x152 size.
     * This method is useful when need to restrict the size of preview frame for some reason (for example for video recording)
     *
     * @param maxWidth  - the maximum width allowed for camera frame.
     * @param maxHeight - the maximum height allowed for camera frame
     */
    public void setMaxFrameSize(int maxWidth, int maxHeight) {
        mMaxWidth = maxWidth;
        mMaxHeight = maxHeight;
    }

    public void SetCaptureFormat(int format) {
        mPreviewFormat = format;
        if (mListener instanceof CvCameraViewListenerAdapter) {
            CvCameraViewListenerAdapter adapter = (CvCameraViewListenerAdapter) mListener;
            adapter.setFrameFormat(mPreviewFormat);
        }
    }

    /**
     * Called when mSyncObject lock is held
     */
    private void checkCurrentState() {
        Log.d(TAG, "call checkCurrentState");
        int targetState;

        if (mEnabled && mSurfaceExist && getVisibility() == VISIBLE) {
            targetState = STARTED;
        } else {
            targetState = STOPPED;
        }

        if (targetState != mState) {
            /* The state change detected. Need to exit the current state and enter target state */
            processExitState(mState);
            mState = targetState;
            processEnterState(mState);
        }
    }

    private void processEnterState(int state) {
        Log.d(TAG, "call processEnterState: " + state);
        switch (state) {
            case STARTED:
                onEnterStartedState();
                if (mListener != null) {
                    mListener.onCameraViewStarted(mFrameWidth, mFrameHeight);
                }
                break;
            case STOPPED:
                onEnterStoppedState();
                if (mListener != null) {
                    mListener.onCameraViewStopped();
                }
                break;
        }
        ;
    }

    private void processExitState(int state) {
        Log.d(TAG, "call processExitState: " + state);
        switch (state) {
            case STARTED:
                onExitStartedState();
                break;
            case STOPPED:
                onExitStoppedState();
                break;
        }
        ;
    }

    private void onEnterStoppedState() {
        /* nothing to do */
    }

    private void onExitStoppedState() {
        /* nothing to do */
    }

    // NOTE: The order of bitmap constructor and camera connection is important for android 4.1.x
    // Bitmap must be constructed before surface
    private void onEnterStartedState() {
        Log.d(TAG, "call onEnterStartedState");
        /* Connect camera */
        if (!connectCamera(getWidth(), getHeight())) {
            AlertDialog ad = new AlertDialog.Builder(getContext()).create();
            ad.setCancelable(false); // This blocks the 'BACK' button
            ad.setMessage("It seems that you device does not support camera (or it is locked). Application will be closed.");
            ad.setButton(DialogInterface.BUTTON_NEUTRAL, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    ((Activity) getContext()).finish();
                }
            });
            ad.show();

        }
    }

    private void onExitStartedState() {
        disconnectCamera();
        if (mCacheBitmap != null) {
            mCacheBitmap.recycle();
        }
    }

    /**
     * This method shall be called by the subclasses when they have valid
     * object and want it to be delivered to external client (via callback) and
     * then displayed on the screen.
     *
     * @param frame - the current frame to be delivered
     */
    Mat canvas = new Mat();

    protected void deliverAndDrawFrame(CvCameraViewFrame frame) {
        Mat modified;
        if (mListener != null) {
            modified = mListener.onCameraFrame(frame);
        } else {
            modified = frame.rgba();
        }
        if (modified != null && modified.channels() == 3) {
            Imgproc.cvtColor(modified, modified, Imgproc.COLOR_BGR2RGBA);
        }

        boolean bmpValid = true;
        if (modified == null || modified.empty()) {
            if (!clean) {
                Canvas canvas = getHolder().lockCanvas();
                if (canvas != null) {
                    canvas.drawColor(0, android.graphics.PorterDuff.Mode.CLEAR);
                    getHolder().unlockCanvasAndPost(canvas);
                }
            }
            clean = true;
            return;
        }
        Rect roiRect = new Rect(0, 0, modified.width(), modified.height());
        if (!mask.empty()) {
            if (matScale != 1) {
                Imgproc.resize(modified, modified, new Size(modified.width() * matScale, modified.height() * matScale));
            }
            int offsetX = (modified.width() - mask.width()) / 2;
            int offsetY = (modified.height() - mask.height()) / 2;
            if (mask.width() >= modified.width()) {
                Imgproc.resize(mask, mask, new Size(modified.width(), mask.height()));
            }
            if (mask.height() >= modified.height()) {
                Imgproc.resize(mask, mask, new Size(mask.width(), modified.height()));
            }
            // 遮罩部分
            Mat roi = new Mat(modified, new org.opencv.core.Rect(offsetX, offsetY, mask.width(), mask.height()));
            Core.bitwise_and(roi, mask, roi);
            roiRect.left = Math.max(0, (modified.width() - mask.width()) / 2);
            roiRect.top = Math.max(0, (modified.height() - mask.height()) / 2);
            roiRect.right = Math.min(modified.width(), roiRect.left + mask.width());
            roiRect.bottom = Math.min(modified.height(), roiRect.top + mask.height());
//            mask.copyTo(modified);
        }
        if (snapCallback != null) {
            Mat tmp = Mat.zeros((int) (offset.y + modified.height()), (int) (offset.x + modified.width()), CvType.CV_8UC4);
            Mat roi = new Mat(modified, new org.opencv.core.Rect(roiRect.left, roiRect.top, roiRect.width(), roiRect.height()));
            roi.copyTo(new Mat(tmp, new org.opencv.core.Rect((int) offset.x, (int) offset.y, roiRect.width(), roiRect.height())));
            snapCallback.snapLast(tmp);
            tmp.release();
            snapCallback = null;
        }
        // 没东西了
        if (mCacheBitmap.getWidth() != modified.width() || mCacheBitmap.getHeight() != modified.height()) {
            Log.i("CameraBridgeViewBase",
                    String.format("ResizeBitmapCache[%d:%d]=>[%d:%d]",
                            mCacheBitmap.getWidth(), mCacheBitmap.getHeight(),
                            modified.width(), modified.height()
                    ));
            mCacheBitmap.recycle();
            mCacheBitmap = Bitmap.createBitmap(modified.width(), modified.height(), Bitmap.Config.ARGB_8888);
        }
        Utils.matToBitmap(modified, mCacheBitmap);
        clean = false;
        modified.release();
        if (bmpValid && mCacheBitmap != null) {
            Canvas canvas = getHolder().lockCanvas();
            if (canvas != null) {
                canvas.drawColor(0, android.graphics.PorterDuff.Mode.CLEAR);
                canvas.drawBitmap(mCacheBitmap, roiRect,
                        new Rect((int) offset.x, (int) offset.y,
                                (int) offset.x + roiRect.width(), (int) offset.y + roiRect.height())
                        , null);
                if (mFpsMeter != null) {
                    mFpsMeter.measure();
                    mFpsMeter.draw(canvas, 20, 30);
                    mFpsMeter.draw(canvas, mFrameWidth, mFrameHeight);
                }
                getHolder().unlockCanvasAndPost(canvas);
            }
        }
    }

    /**
     * This method is invoked shall perform concrete operation to initialize the camera.
     * CONTRACT: as a result of this method variables mFrameWidth and mFrameHeight MUST be
     * initialized with the size of the Camera frames that will be delivered to external processor.
     *
     * @param width  - the width of this SurfaceView
     * @param height - the height of this SurfaceView
     */
    protected abstract boolean connectCamera(int width, int height);

    /**
     * Disconnects and release the particular camera object being connected to this surface view.
     * Called when syncObject lock is held
     */
    protected abstract void disconnectCamera();

    // NOTE: On Android 4.1.x the function must be called before SurfaceTextre constructor!
    protected void AllocateCache() {
//        if (mScale != 0) {
//            mCacheBitmap = Bitmap.createBitmap((int) (mFrameWidth * mScale), (int) (mFrameHeight * mScale), Bitmap.Config.ARGB_8888);
//        } else {
        mCacheBitmap = Bitmap.createBitmap(mFrameWidth, mFrameHeight, Bitmap.Config.ARGB_8888);
//        }
    }

    public interface ListItemAccessor {
        public int getWidth(Object obj);

        public int getHeight(Object obj);
    }

    ;

    /**
     * This helper method can be called by subclasses to select camera preview size.
     * It goes over the list of the supported preview sizes and selects the maximum one which
     * fits both values set via setMaxFrameSize() and surface frame allocated for this view
     *
     * @param supportedSizes
     * @param surfaceWidth
     * @param surfaceHeight
     * @return optimal frame size
     */
    protected Size calculateCameraFrameSize(List<?> supportedSizes, ListItemAccessor accessor, int surfaceWidth, int surfaceHeight) {
        int calcWidth = 0;
        int calcHeight = 0;

        int maxAllowedWidth = (mMaxWidth != MAX_UNSPECIFIED && mMaxWidth < surfaceWidth) ? mMaxWidth : surfaceWidth;
        int maxAllowedHeight = (mMaxHeight != MAX_UNSPECIFIED && mMaxHeight < surfaceHeight) ? mMaxHeight : surfaceHeight;

        for (Object size : supportedSizes) {
            int width = accessor.getWidth(size);
            int height = accessor.getHeight(size);

            if (width <= maxAllowedWidth && height <= maxAllowedHeight) {
                if (width >= calcWidth && height >= calcHeight) {
                    calcWidth = (int) width;
                    calcHeight = (int) height;
                }
            }
        }

        return new Size(calcWidth, calcHeight);
    }


    public void regSnapCallback(SnapCallback snapCallback) {
        this.snapCallback = snapCallback;
    }
}
